import mbuild as mb
import sys

#making a benzene with 3 carbon tail

class benzene(mb.Compound):
    def __init__(self):
        super(benzene, self).__init__()
        
        #add benzene
        mb.load('benzene.mol2', compound=self)
        self.label_rigid_bodies()
        mb.translate(self, -self[0].pos)#move [0] carbon to origin
        
        #add port
        port = mb.Port(anchor=self[0])
        self.add(port, label='benzene_port')
        
        #move port half a C-C bond length away from carbon
        #mb.translate(self['benzene_port'], [0, -0.07, 0])

class tail(mb.Compound):
    def __init__(self):
        super(tail, self).__init__()
        
        #import tail
        mb.load('tail1.mol2', compound=self)
        mb.translate(self, -self[0].pos) #move [0] carbon to origin
        
        #add port
        port = mb.Port(anchor=self[0])
        self.add(port, label='tail_port')
        
        #move port half a C-C bond length away from carbon
        mb.translate(self['tail_port'], [0.14, -0.07, 0])

class bt(mb.Compound):
    def __init__(self):
        super(bt, self).__init__()
        self.add(benzene(), label='benzene')
        self.add(tail(), label='tail')
        mb.force_overlap(move_this=self['benzene'],
                        from_positions=self['benzene']['benzene_port'],
                        to_positions=self['tail']['tail_port'])

mycomp = bt()

N=int(sys.argv[1])
filled_box = mb.packing.fill_box(mycomp, density=350.0, n_compounds=N, fix_orientation=True)
filled_box.save('bt_box.hoomdxml', overwrite=True, ref_distance=2.8, ref_mass=12.011)


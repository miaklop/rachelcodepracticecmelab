import hoomd
import hoomd.md
from cme_utils.manip.convert_rigid import init_wrapper
hoomd.context.initialize("")

steps = 6e5 
T = 0.01
stretchy = 100

system = init_wrapper('bt_box.hoomdxml')

nl = hoomd.md.nlist.tree()
lj = hoomd.md.pair.lj(r_cut=2.5, nlist=nl)

print(system.particles.types)

lj.set_params(mode='xplor')
lj.pair_coeff.set(system.particles.types, [i for (i, v) in zip(system.particles.types, [_.startswith("_R0") for _ in system.particles.types]) if v], epsilon=0.0, sigma=0.0, r_cut=0)
lj.pair_coeff.set('C', 'C', epsilon=1, sigma=1)

harmonic = hoomd.md.bond.harmonic()
harmonic.bond_coeff.set('C-C', k=stretchy, r0=0.55) #assuming C-C bond 1.54 A, ref_distance=2.8
#k sets the rigidity of the bond

hoomd.md.integrate.mode_standard(dt=0.005)
rigid = hoomd.group.rigid_center()
rest = hoomd.group.nonrigid()
both = hoomd.group.union('both', rigid, rest)
hoomd.md.integrate.nvt(group=both, kT=T, tau=0.5)

T=str(T)
hoomd.analyze.log(filename="port1-output.log",
			quantities=['potential_energy',
					'temperature'],
			period=100,
			overwrite=True)
hoomd.dump.gsd("port1sim.gsd",
		period=1000,
		group=hoomd.group.all(),
		overwrite=True)
hoomd.run(steps)

#Import hoomd into the file and initialize the execution context
import hoomd
import hoomd.md
hoomd.context.initialize("")

#Create the lattice
hoomd.init.create_lattice(unitcell=hoomd.lattice.sc(a=2.0), n=5)

#Begin defining the potential energy using lennard-jones potential
n1 = hoomd.md.nlist.cell()
lj = hoomd.md.pair.lj(r_cut=2.5, nlist = n1)
lj.pair_coeff.set('A', 'A', epsilon=1.0, sigma=1.0)

#Select an integrator
hoomd.md.integrate.mode_standard(dt=0.005)
all = hoomd.group.all()
hoomd.md.integrate.langevin(group=all, kT=0.2, seed=42)

#Put the data into two files: one is for numbers and the other is for running the simulation on VMD
hoomd.analyze.log(filename="log-output.log",
                 quantities=['potential_energy', 'temperature'],
                 period=100,
                 overwrite=True)
hoomd.dump.gsd("trajectory.gsd", period=2e3, group=all, overwrite=True)

#Run the simulation
hoomd.run(1e4)
print("simulation done")

#Create an array called data for plotting our output
import numpy
from matplotlib import pyplot
data = numpy.genfromtxt(fname='log-output.log', skip_header=True)

#Plot the data in the second column
pyplot.figure(figsize=(10,5), dpi=140)
pyplot.plot(data[:,0], data[:,1])
pyplot.xlabel('time step')
pyplot.ylabel('potential_energy')
pyplot.savefig("PE.png")

#Plot the data in the third column
pyplot.figure(figsize=(10,5), dpi=140)
pyplot.plot(data[:,0], data[:,2])
pyplot.xlabel('time step')
pyplot.ylabel('temperature')
pyplot.savefig("Temp.png")

import mbuild as mb
import sys
# benzene with a three carbon tail

class benzene(mb.Compound):
    def __init__(self):
        super(benzene, self).__init__()
        mb.load('benzene.mol2', compound=self)
        self.label_rigid_bodies()
        mb.translate(self, -self[0].pos)
        port = mb.Port(anchor=self[0])
        self.add(port, label='up')
                            
class tail(mb.Compound):
    def __init__(self):
        super(tail, self).__init__()
        mb.load('c3.mol2', compound=self)
        mb.translate(self, -self[0].pos)
        port = mb.Port(anchor=self[2])
        self.add(port, label='down')
        mb.translate(self['down'], [0.14, -0.07, 0])

class bt(mb.Compound):
    def __init__(self):
        super(bt, self).__init__()
        self.add(benzene(), label='benzene')
        self.add(tail(), label='tail')
        mb.force_overlap(move_this=self['benzene'],
                from_positions=self['benzene']['up'],
                to_positions=self['tail']['down'])

mycomp = bt()
mycomp.save('bt.hoomdxml', overwrite=True)

N=int(sys.argv[1])
filled_box = mb.packing.fill_box(mycomp, density=862.0, n_compounds=N, fix_orientation=True)
filled_box.save('bt_box.hoomdxml', overwrite=True, ref_distance=2.8, ref_mass=12.017)

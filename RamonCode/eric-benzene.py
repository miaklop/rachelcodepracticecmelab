#Importing packages that will be needed for making the code work correctly. 
import hoomd
import hoomd.md
#This helps us deal with hoomd thinking (0,0,0) is in one place and _______ thinking it is in a different place. 
from cme_utils.manip.convert_rigid import init_wrapper
#Initializes the system
hoomd.context.initialize("")

#Some important variables. Makes it easier to change things! 
steps = 6e5
dt = 0.005
#When temperature is changed, a new file with the output will be created instead of overwriting the existing one. 
T = 3 #Should be liquid (unless it's too cold!!)

#Fixes our (0,0,0) problem
#This file is created in box.py
system = init_wrapper('benzene_box.hoomdxml')

#Using a different neighborlist instead of cell.
#Tree neighborlists are useful when there is a large size disparity.
nl = hoomd.md.nlist.tree()###################################################################################

#We are using Lennard-Jones Potential for our interactions. 
lj = hoomd.md.pair.lj(r_cut=2.5, nlist=nl)

#This sets a way to control how forces are computed
#'xplor' is "a smoothing function [that] is applied to gradually decrease both the force and potential to 0 at the cutoff [value]"
#Basically keeps the LJ function from doing a hard stop
lj.set_params(mode='xplor')

#This sets potential coefficients between one type pair
#The outermost brackets are creating a list using 'list comprehension'
#'Zip' takes in parameters of particle types(a list) and another list - it will traverse the lists at the same time and return those values
#The innermmost brackets should return a list that is [False] because the only particle type is 'C'
#First time through the outermost brackets, we get [C, False]
#system.particles.types = [C]
#'if v' is actually 'if v = True'
#Basically the whole list comprehension thing ends up with an empty list so nothing has a 0 epsilon, sigma, and r_cut

#SO, THIS CODE DOESN'T ACTUALLY DO ANYTHING SO WHY IS THIS HERE
lj.pair_coeff.set(system.particles.types, [i for (i, v) in zip(system.particles.types, [b.startswith("_R") for b in system.particles.types]) if v], epsilon=0.0, sigma=0.0, r_cut=0)

#Only one type of particle so only one force is needed. Using the general epsilon=1 and sigma=1
#Since epilon and sigma as both 1, we have simulated all 5-atom, ring compounds because we can now just scale everything up
lj.pair_coeff.set('C','C', epsilon=1, sigma=1)
hoomd.md.integrate.mode_standard(dt=dt)

#Using the langevin equations to describe motion
#'group' is a group of particles that are the centers of rigid bodies unlike group.all() which is a group of all particles
#We use rigid_center because we only want to update the whole compound, not each of the constituent atoms. (Will be angry if you don't do this)
group = hoomd.group.rigid_center()
integrater = hoomd.md.integrate.langevin(group=group, kT=T, seed=42)

#Creates output files depending on the temperature value provided
T = str(T)
hoomd.analyze.log(filename=T+"-ring3-output.log",
                quantities=['potential_energy', 'temperature'],
                period=100,
                overwrite=True)
hoomd.dump.gsd(T+"-ring3.gsd",
                period=1000,
                group=hoomd.group.all(),
                overwrite=True)

#Runs the simulation!
hoomd.run(steps)

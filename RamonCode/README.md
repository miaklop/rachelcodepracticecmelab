# Software Needed
To run these simulations you will need the master branch of [mBuild](https://github.com/mosdef-hub/mbuild): you will want to make a software diretory to store these files


``` Bash
git clone https://github.com/mosdef-hub/mbuild.git
cd mbuild
pip install -e .
```

And to install the mbuild deps:

``` Bash
conda install --only-deps -c omnia -c mosdef mbuild


You will also need `cme_utils`

``` Bash
git clone git@bitbucket.org:cmelab/cme_utils.git
cd cme_utils
pip install -e .
```

You may also need

```Bash
conda install -c glotzer gsd signac
```
for cme_utils to work


# How to run simulations:
To run the simulation you will first want to create the box with `python box.py` and specify the number of particles (i.e. `python box.py 75` wil    l give a box with 75 particles.
Then you can use `python eric-benzene.py` or `python benzene2.py` to run the simulation. These both do the same thing but, benzene2.py is modifie    d to change the temperature on the command line.

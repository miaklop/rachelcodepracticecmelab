#Importing packages that will be useful
import mbuild as mb
import sys

#You have to specify the number of particles that you want in the command line. Must be above 50.
N=int(sys.argv[1])

#This is loading in a file that contains benzene so that it doesn't have to created each time code is written
#This was a file that Ramon made in Avogadro
benzene = mb.load('benzene.mol2')

#Designates which compounds are considered rigid bodies. Since no arguments are provided, it treats the entire compound as one rigid body.
benzene.label_rigid_bodies()

#n_compounds - an int; the number of compounds to be put in the box
#density - a float; target density in macroscale units
#Using the two above parameters, a box size will be calculated and filled with the compound
#fis_orientation - keeps the compounds from being rotated when put in the box
#Yu can also specify a box but then only one of n_compounds or density must be specified (always need two of the three)
filled_box = mb.packing.fill_box(benzene, density=876.5, n_compounds=N, fix_orientation=True)

#Saves the box of benzene to a file. Everytime a new one is created, it will be overwritten. 
#ref_distance - converts distance values to reduced units. Helpful for saving to .gsd or .hoomdxml (width of a Carbon atom)
#ref_mass - converts mass values to reduced units. Helpful for saving to .gsd or hoomd.xml (mass of a Carbon atom in amu)
#The units become dimensionless which makes our lives easier. 
filled_box.save('benzene_box.hoomdxml', overwrite=True, ref_distance=2.8, ref_mass=12.017)

